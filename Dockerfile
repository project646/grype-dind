FROM docker:19.03-git

RUN apk update && \
    apk add curl && \
    rm -rf /var/cache/apk/*

RUN curl -sSfL https://raw.githubusercontent.com/anchore/grype/main/install.sh | sh -s -- -b /usr/local/bin
